# EAS
Emotions Analytic System (EAS) on Instagram social network data

Nowadays, thanks to spread of social media, and huge amount of data in Internet, the need for changing how we look and interpret data is evolving. Visualization is one of the most important fields in data science. About growing usage of social media, analyzing the data they contain is crucial. In this research, I developed and deployed an emotions analytic system for Instagram in Persian. In this system, I analyze emotions and words that user writes, and visualize them by visualizing techniques. Over 370,000 Instagram comments have been collected with the help of data crawlers that we developed, after that I prepared the data and preprocessed them as my data; including normalizing, finding the keywords and etc. I developed this system by python.

This Dataset has over 370.000 comments (that most of them are in Persian) from 40 instagram channels. These comments are crawled from 12 April 2017 (1396/01/26 A.H.S) to 29 July 2017 (1396/05/07 A.H.S).
